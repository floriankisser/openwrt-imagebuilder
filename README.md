# OpenWRT Image Builder

Build OpenWRT Image Builder container image and build some OpenWRT images with GitLab CI.

## Container image

Contains OpenWRT image builder for a specific target.

Usage:

```sh
podman run --rm registry.gitlab.com/floriankisser/openwrt-imagebuilder
```

List available target profiles:

```sh
podman run --rm registry.gitlab.com/floriankisser/openwrt-imagebuilder info
```

## Wrapper script

Build and collect images with custom packages locally, using image builder container from registry:

```shell
./make-image.sh
```

## Build container image locally

Run as unprivileged user, produces `localhost/openwrt-imagebuilder`.

```sh
buildah unshare ./build.sh
```
