#!/usr/bin/env sh

set -e
. ./env.sh

image=$CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH
build_target=./build/targets/
profile=$1
packages="luci dnsmasq-full -dnsmasq wpad -wpad-basic-mbedtls ip-full luci-proto-wireguard"

if [ -z "$profile" ]; then
  podman run --rm "$image" info
  exit 1
fi

mkdir -p $build_target
podman run --rm -v "$build_target:/openwrt-imagebuilder/bin/targets/:z" "$image" image PROFILE="$profile" PACKAGES="$packages"
