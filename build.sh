#!/usr/bin/env sh

set -ex
. ./env.sh

version=23.05.3
directory=$CI_COMMIT_BRANCH
target=$(echo "$CI_COMMIT_BRANCH" | sed "s/\//-/")
release=https://downloads.openwrt.org/releases/${version}/targets/${directory}/openwrt-imagebuilder-${version}-${target}.Linux-x86_64.tar.xz

base=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base/base
toolbox=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base/toolbox
cache=$(pwd)/build/cache
dnf_cache=$cache/dnf

mkdir -p "$dnf_cache"

ctr=$(buildah from "$base")
mnt=$(buildah mount "$ctr")
tools=$(buildah from -v "$mnt":/centos_root -v /dev/null:/centos_root/dev/null -v "$dnf_cache":/centos_root/var/cache/dnf:z "$toolbox")

buildah run "$tools" -- dnf -y --installroot=/centos_root install \
  bzip2 \
  diffutils \
  file \
  findutils \
  git \
  make \
  patch \
  perl-Thread-Queue \
  perl-Data-Dumper \
  python2 \
  python3 \
  tar \
  unzip \
  wget \
  which \

buildah run "$tools" -- curl --proto '=https' --tlsv1.2 -sSf $release > "${cache}/openwrt-imagebuilder.tar.xz"

buildah rm "$tools"

buildah add "$ctr" "${cache}/openwrt-imagebuilder.tar.xz" /
buildah run "$ctr" sh -c "mv /openwrt-imagebuilder-* /openwrt-imagebuilder"

buildah config --workingdir /openwrt-imagebuilder "$ctr"
buildah config --entrypoint "[\"make\"]" "$ctr"
buildah config --volume /openwrt-imagebuilder/bin/targets/ "$ctr"
buildah config --label "name=openwrt-imagebuilder" "$ctr"
buildah config --label "commit=$CI_COMMIT_SHA" "$ctr"

buildah commit --rm "$ctr" openwrt-imagebuilder
