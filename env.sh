#!/usr/bin/env sh

if [ -z "$CI" ]; then
  CI_REGISTRY=registry.gitlab.com
  CI_PROJECT_ROOT_NAMESPACE=floriankisser
  CI_REGISTRY_IMAGE=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/openwrt-imagebuilder
  CI_COMMIT_SHA=$(git rev-parse HEAD)
  CI_COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
  CI_COMMIT_REF_SLUG=$(echo "$CI_COMMIT_BRANCH" | tr '[:upper:]' '[:lower:]' | sed "s/[^a-z0-9]/-/g" | cut -b1-63)
fi
